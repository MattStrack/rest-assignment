package com.strack.restservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@Component
public class CustomerService {

    @Autowired
    JdbcTemplate jdbcTemplate;

    //sets up the database
    public void tableSetup() {
        jdbcTemplate.execute("DROP TABLE customers IF EXISTS");
        jdbcTemplate.execute("CREATE TABLE customers(" + "id  SERIAL, first_name VARCHAR(255), last_name VARCHAR(255))");

        jdbcTemplate.execute("INSERT INTO customers(first_name, last_name) VALUES('Chad','Austin')");
        jdbcTemplate.execute("INSERT INTO customers(first_name, last_name) VALUES('Noah','Whitehead')");
        jdbcTemplate.execute("INSERT INTO customers(first_name, last_name) VALUES('EJ','Chong')");
        jdbcTemplate.execute("INSERT INTO customers(first_name, last_name) VALUES('Sam','Lopez')");
        jdbcTemplate.execute("INSERT INTO customers(first_name, last_name) VALUES('Thomas','Cassa')");
        jdbcTemplate.execute("INSERT INTO customers(first_name, last_name) VALUES('Isaac','Herriges')");
    }

    //returns a list of all the customers in the database
    public ArrayList<Customer> retrieveCustomers() {
        ArrayList<Customer> customerList = new ArrayList<>();
        jdbcTemplate.query("SELECT id, first_name, last_name FROM customers", (rs, rowNum) -> new Customer(rs.getLong("id"), rs.getString("first_name"), rs.getString("last_name"))).forEach(customer -> customerList.add(customer));
        return customerList;
    }

    //adds a single customer to the database
    public Customer addCustomer(String firstName, String lastName) {
        jdbcTemplate.update("INSERT INTO customers(first_name, last_name) VALUES(?, ?)", new Object[] {firstName}, new Object[] {lastName});
        return new Customer(firstName, lastName);
    }

    //returns a single customer from the database based on a given Id number
    public Customer retrieveCustomer(Long id) {
        ArrayList<Customer> customerUnique = new ArrayList<>();
		jdbcTemplate.query("SELECT id, first_name, last_name FROM customers WHERE id=?", new Object[] {id}, (rs, rowNum) -> new Customer(rs.getLong("id"),
				rs.getString("first_name"), rs.getString("last_name"))).forEach(customer -> customerUnique.add(customer));

        return customerUnique.get(0);
    }

    //Updates a customers name in the database based on ID provided
    public Customer updateCustomer(Long id, Customer newCustomer) {
		String updateQuery = "UPDATE customers SET first_name=?, last_name=? WHERE id=?";
		jdbcTemplate.update(updateQuery, newCustomer.getFirstName(), newCustomer.getLastName(), id);

		return new Customer(newCustomer.getFirstName(), newCustomer.getLastName());
    }

    //Deletes a customer in the database based on ID provided
    public Customer deleteCustomer(Long id) {
        Customer customerDeleted = this.retrieveCustomer(id);

        String deleteQuery = "DELETE FROM customers WHERE id=?";
		jdbcTemplate.update(deleteQuery, id);

		return customerDeleted;
    }

}
