package com.strack.restservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class RestServiceApplication implements CommandLineRunner {

	public static void main(String[] args) {

		SpringApplication.run(RestServiceApplication.class, args);
	}

	@Autowired
	CustomerService customerService;

	//initial run method, sets up the database of customers
	@Override
	public void run(String... strings) throws Exception {
		customerService.tableSetup();
	}
}
