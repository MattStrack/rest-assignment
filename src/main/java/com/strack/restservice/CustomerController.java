package com.strack.restservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;

@RestController
public class CustomerController {

    @Autowired
    CustomerService customerService;

    //GET request - need this to return all customers in the database
    @GetMapping("/customers")
    public ArrayList<Customer> retrieveCustomers() {
        return customerService.retrieveCustomers();
    }

    //POST request - adds a customer to the database based on input parameters
    @PostMapping("/customers")
    public Customer createCustomer(@RequestParam(value="firstName", defaultValue = "n/a") String firstName,
                                   @RequestParam(value="lastName", defaultValue = "n/a") String lastName) {
        return customerService.addCustomer(firstName, lastName);
    }

    //GET request - return a single customer based on an ID provided in the URL
    @GetMapping("/customers/{id}")
    public Customer retrieveCustomer(@PathVariable Long id) {
        return customerService.retrieveCustomer(id);
    }

    //PUT request - put a customer in the database based on ID provided in the URL and some input parameters
    //note this should not be putting in the same customer multiple times
    @PutMapping("/customers/{id}")
    public Customer updateCustomer(@RequestBody Customer customer, @PathVariable Long id) {
        return customerService.updateCustomer(id, customer);
    }

    //DELETE request - deletes a customer in the database based on ID provided in the URL
    @DeleteMapping("/customers/{id}")
    public Customer deleteCustomer(@PathVariable Long id) {
        return customerService.deleteCustomer(id);
    }


}
